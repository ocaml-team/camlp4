Source: camlp4
Section: ocaml
Priority: optional
Maintainer: Debian OCaml Maintainers <debian-ocaml-maint@lists.debian.org>
Uploaders:
 Stéphane Glondu <glondu@debian.org>,
 Ximin Luo <infinity0@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 ocaml (>= 5.3),
 ocamlbuild (>= 0.10.1),
 ocaml-findlib,
 libcamlp-streams-ocaml-dev,
 dh-ocaml
Standards-Version: 4.7.0
Rules-Requires-Root: no
Homepage: https://github.com/ocaml/camlp4
Vcs-Browser: https://salsa.debian.org/ocaml-team/camlp4
Vcs-Git: https://salsa.debian.org/ocaml-team/camlp4.git

Package: camlp4
Architecture: any
Depends:
 libcamlp4-ocaml-dev,
 ${shlibs:Depends},
 ${misc:Depends},
 ${ocaml:Depends}
Provides:
 camlp4-extra,
 ${ocaml:Provides}
Recommends: ocaml-findlib
Breaks: camlp4-extra (<< 4.03)
Replaces: camlp4-extra (<< 4.03)
Description: Pre Processor Pretty Printer for OCaml
 Camlp4 is a software system for writing extensible parsers for
 programming languages. It provides a set of OCaml libraries that are
 used to define grammars as well as loadable syntax extensions of such
 grammars. Camlp4 stands for Caml Preprocessor and Pretty-Printer and
 one of its most important applications is the definition of
 domain-specific extensions of the syntax of OCaml.
 .
 This package contains CamlP4 executables for pre-processing and
 pretty-printing OCaml sources both interactively and in a batch
 fashion.

Package: libcamlp4-ocaml-dev
Architecture: any
Depends:
 ${shlibs:Depends},
 ${misc:Depends},
 ${ocaml:Depends}
Provides:
 ${ocaml:Provides}
Recommends: ocaml-findlib
Breaks: camlp4 (<< 4.03)
Replaces: camlp4 (<< 4.03)
Description: Pre Processor Pretty Printer for OCaml (libraries)
 Camlp4 is a software system for writing extensible parsers for
 programming languages. It provides a set of OCaml libraries that are
 used to define grammars as well as loadable syntax extensions of such
 grammars. Camlp4 stands for Caml Preprocessor and Pretty-Printer and
 one of its most important applications is the definition of
 domain-specific extensions of the syntax of OCaml.
 .
 This package contains CamlP4 libraries.
